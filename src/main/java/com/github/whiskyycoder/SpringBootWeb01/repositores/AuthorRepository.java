package com.github.whiskyycoder.SpringBootWeb01.repositores;

import com.github.whiskyycoder.SpringBootWeb01.model.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Long> {
}
