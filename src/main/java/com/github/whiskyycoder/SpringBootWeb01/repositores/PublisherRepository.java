package com.github.whiskyycoder.SpringBootWeb01.repositores;

import com.github.whiskyycoder.SpringBootWeb01.model.Publisher;
import org.springframework.data.repository.CrudRepository;

public interface PublisherRepository extends CrudRepository<Publisher, Long> {
}
