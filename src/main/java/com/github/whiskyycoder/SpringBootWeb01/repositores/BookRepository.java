package com.github.whiskyycoder.SpringBootWeb01.repositores;

import com.github.whiskyycoder.SpringBootWeb01.model.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book,Long> {
}
