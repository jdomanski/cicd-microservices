package com.github.whiskyycoder.SpringBootWeb01.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Publisher {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String address;


    @OneToMany(mappedBy = "publisher")
    private Set<Book> books;

    public Publisher(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public Publisher setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Publisher setName(String name) {
        this.name = name;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Publisher setAddress(String address) {
        this.address = address;
        return this;
    }

    public Set<Book> getBooks() {
        return books;
    }

    public Publisher setBooks(Set<Book> books) {
        this.books = books;
        return this;
    }
}
