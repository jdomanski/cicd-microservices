package com.github.whiskyycoder.SpringBootWeb01.Controllers;

import com.github.whiskyycoder.SpringBootWeb01.repositores.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AuthorController {
    @Autowired
    AuthorRepository authorRepository;


    @GetMapping("/authors")
    public String getAuthors(Model model){

        model.addAttribute("authors",authorRepository.findAll());

        return "Authors";
    }
}
