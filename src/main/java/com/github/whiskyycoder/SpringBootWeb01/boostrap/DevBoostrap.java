 package com.github.whiskyycoder.SpringBootWeb01.boostrap;

import com.github.whiskyycoder.SpringBootWeb01.model.Author;
import com.github.whiskyycoder.SpringBootWeb01.model.Book;
import com.github.whiskyycoder.SpringBootWeb01.model.Publisher;
import com.github.whiskyycoder.SpringBootWeb01.repositores.AuthorRepository;
import com.github.whiskyycoder.SpringBootWeb01.repositores.BookRepository;
import com.github.whiskyycoder.SpringBootWeb01.repositores.PublisherRepository;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class DevBoostrap{

    private AuthorRepository authorRepository;
    private BookRepository bookRepository;
    private PublisherRepository publisherRepository;

    public DevBoostrap(AuthorRepository authorRepository, BookRepository bookRepository, PublisherRepository publisherRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository=publisherRepository;
    }


    @EventListener
    public void handleContextRefresh(ContextRefreshedEvent event) {
        initData();
    }

    private void initData(){

        //Eric
        Author eric = new Author("Eric", "Evans");
        Publisher publisher=new Publisher("Nowa era","Comercial Street");
        Book  ddd = new Book("Domain Driven Design", "1234", null );
        eric.getBooks().add(ddd);
        authorRepository.save(eric);
        publisherRepository.save(publisher);
        //bookRepository.save(ddd);




        //Rod
        Author rod = new Author("Rod", "Johnson");
        //Book noEJB = new Book("J2EE Development without EJB", "23444", "Wrox" );
        //rod.getBooks().add(noEJB);
        //noEJB.getAuthors().add(rod);

        authorRepository.save(rod);
        //bookRepository.save(noEJB);
    }}